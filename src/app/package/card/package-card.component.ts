import { Component, OnInit, Input } from "@angular/core";
import { Package } from "src/app/models/package";

@Component({
  selector: "app-package-card",
  templateUrl: "./package-card.component.html",
  styleUrls: ["./package-card.component.css"]
})
export class PackageCardComponent implements OnInit {
  @Input()
  package: Package;

  constructor() {}

  ngOnInit() {}
}
