import { Component, OnInit, OnDestroy } from "@angular/core";
import { PackageService } from "../services/package.service";
import { Observable } from "rxjs";
import { Package } from "../models/package";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-package",
  templateUrl: "./package.component.html",
  styleUrls: ["./package.component.css"]
})
export class PackageComponent implements OnInit, OnDestroy {
  package$: Observable<Package>;
  private sub: any;

  constructor(private service: PackageService, private route: ActivatedRoute) {}
  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      console.log(params);
      const name = params["name"]; // (+) converts string 'id' to a number
      const version = params["version"];

      this.package$ = this.service.show(name, version);
      // In a real app: dispatch action to load the details here.
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
