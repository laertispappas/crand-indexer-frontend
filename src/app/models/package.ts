class User {
  name: String;
  email: String;
}

export class Package {
  name: string;
  title: string;
  description: string;
  version: string;

  authors: User[];
  maintainers: User[];

  constructor(attributes: {}) {
    const keys = Object.keys(attributes);
    keys.forEach(key => {
      this[key] = attributes[key];
    });
  }
}
