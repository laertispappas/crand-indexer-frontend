import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { Package } from "../models/package";
import { PackageService } from "../services/package.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  packages$: Observable<Package[]>;

  constructor(private packageService: PackageService) {}

  ngOnInit() {
    this.packages$ = this.packageService.list();
  }
}
