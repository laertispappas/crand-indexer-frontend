import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Package } from "../models/package";
import { Observable } from "rxjs";

import { environment } from "../../environments/environment";

import { map } from "rxjs/operators";

@Injectable()
export class PackageService {
  constructor(private http: HttpClient) {}

  list(): Observable<Package[]> {
    const path = this.resourcePath("packages");

    return this.http.get(path).pipe(
      map((res: Object[]) => {
        return res.map(p => new Package(p));
      })
    );
  }

  show(name, version): Observable<Package> {
    const path = this.resourcePath(`packages/${name}?version=${version}`);
    return this.http.get(path).pipe(map(res => new Package(res)));
  }

  private resourcePath(path) {
    return `${environment.apiBaseUrl}/${path}`;
  }
}
