import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";

import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FlexLayoutModule } from "@angular/flex-layout";
import { MaterialModule } from "./material.module";
import { RouterModule, Routes } from "@angular/router";

import { AppComponent } from "./app.component";
import { HomeComponent } from "./home/home.component";
import { PackageComponent } from "./package/package.component";
import { PackageService } from "./services/package.service";
import { PackageCardComponent } from "./package/card/package-card.component";

const appRoutes: Routes = [
  { path: "", component: HomeComponent },
  { path: "packages/:name/:version", component: PackageComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PackageComponent,
    PackageCardComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,

    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    MaterialModule,
    FlexLayoutModule
  ],
  providers: [PackageService],
  bootstrap: [AppComponent]
})
export class AppModule {}
