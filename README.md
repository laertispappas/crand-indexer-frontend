# CranIndexerFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.4.

## Development server

Make sure the following dependencies are installed
* [Node version 8.x or 10.x.](https://nodejs.org/en/)
* [Angular: 6](https://angular.io/guide/quickstart)

Inside the parent directory (`cran-indexer-frontend`) run

* npm install

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.

Make sure the [api](https://bitbucket.org/laertispappas/crand-indexer-backend/src/master/) is running.
